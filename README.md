************************************************************
*******presentation link https://youtu.be/4OYUHEgFTcI*******
************************************************************


As part of the two-column template, add a container in which you will display weather information:
City Name
current temperature
current pressure
wind power

Temperature is to be given in degrees Celsius, pressure in hPa and wind in km / h.

Download weather information from the REST API offered by https://openweathermap.org/api
You can use the API for free, do not exceed 60 calls per minute

The API reference is to be made asynchronously (AJAX).
Below are links to make such a call using the jQuery library that you have implemented with Twitter Bootstrap
https://kursjs.pl/kurs/jquery/jquery-ajax.php
https://api.jquery.com/jquery.ajax/

On the basis of weather information, display the appropriate icon that will describe the current weather (similarly to forecasts). 
You can download the icons for free from https://www.flaticon.com/search?word=weather

Remember about responsiveness.

Handling boundary conditions such as no data returned, server unavailability etc.

The location is to be transferred to the API. Free form, city selector, field to enter postal code or city name. 
Those who want to know a useful and meaningful tool recommend integration with Google Maps API, which will also be completely free.
It will allow you to download information about your current location, and then transfer it to the specified API.